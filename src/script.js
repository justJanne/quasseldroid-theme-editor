const colors = [
"colorPrimary", "colorPrimaryDark", "colorAccent",

"senderColor0", "senderColor1", "senderColor2", "senderColor3", "senderColor4", "senderColor5", "senderColor6", "senderColor7", "senderColor8", "senderColor9", "senderColorA", "senderColorB", "senderColorC", "senderColorD", "senderColorE", "senderColorF",

"colorTextToolbarPrimary", "colorTextToolbarSecondary",
"colorTextPrimary", "colorTextPrimaryInverse", "colorTextSecondary", "colorTextSecondaryInverse", "colorDivider",
"colorForeground", "colorForegroundHighlight", "colorForegroundHighlightSecondary", "colorForegroundSecondary", "colorForegroundAction", "colorForegroundNotice", "colorForegroundError",
"colorBackground", "colorBackgroundHighlight", "colorBackgroundSecondary", "colorBackgroundCard", "colorBackgroundDialog",
"colorMarkerLine",
"colorTintActivity", "colorTintMessage", "colorTintHighlight", "colorTintNotification",
"colorTintSecure", "colorTintPartiallySecure", "colorTintInsecure",
];

const themes = ["MaterialLight", "MaterialDark", "QuasselLight", "QuasselDark", "Amoled", "SolarizedLight", "SolarizedDark", "GruvboxLight", "GruvboxDark", "Dracula"];
const themeData = {
  "MaterialLight": {"colorPrimary":"#0a70c0","colorPrimaryDark":"#105a94","colorAccent":"#ffaf3b","senderColor0":"#f44336","senderColor1":"#2196f3","senderColor2":"#7cb342","senderColor3":"#7b1fa2","senderColor4":"#da8e00","senderColor5":"#4caf50","senderColor6":"#3f51b5","senderColor7":"#e91e63","senderColor8":"#b94600","senderColor9":"#9e9d24","senderColorA":"#558b2f","senderColorB":"#009688","senderColorC":"#0277bd","senderColorD":"#00838f","senderColorE":"#9c27b0","senderColorF":"#c51162","colorTextToolbarPrimary":"#ffffff","colorTextToolbarSecondary":"#b2ffffff","colorTextPrimary":"#de000000","colorTextPrimaryInverse":"#ffffff","colorTextSecondary":"#8a000000","colorTextSecondaryInverse":"#b2ffffff","colorDivider":"#1f000000","colorForeground":"#de000000","colorForegroundHighlight":"#de000000","colorForegroundHighlightSecondary":"#8a000000","colorForegroundSecondary":"#8a000000","colorForegroundAction":"#01579b","colorForegroundNotice":"#b56a00","colorForegroundError":"#b71c1c","colorBackground":"#fafafa","colorBackgroundHighlight":"#40ffaf3b","colorBackgroundSecondary":"#f0f0f0","colorBackgroundCard":"#ffffff","colorBackgroundDialog":"#fafafa","colorMarkerLine":"#b71c1c","colorTintActivity":"#afb42b","colorTintMessage":"#1976d2","colorTintHighlight":"#ffab00","colorTintNotification":"#D32F2F","colorTintSecure":"#4caf50","colorTintPartiallySecure":"#ffc107","colorTintInsecure":"#d32f2f"},
  "MaterialDark": {"colorPrimary":"#0a70c0","colorPrimaryDark":"#105a94","colorAccent":"#ffaf3b","senderColor0":"#F44336","senderColor1":"#2196F3","senderColor2":"#8BC34A","senderColor3":"#673AB7","senderColor4":"#FFC107","senderColor5":"#4CAF50","senderColor6":"#3F51B5","senderColor7":"#E91E63","senderColor8":"#b94600","senderColor9":"#CDDC39","senderColorA":"#558B2F","senderColorB":"#009688","senderColorC":"#0277BD","senderColorD":"#00838F","senderColorE":"#9C27B0","senderColorF":"#C51162","colorTextToolbarPrimary":"#ffffff","colorTextToolbarSecondary":"#b2ffffff","colorTextPrimary":"#ffffffff","colorTextPrimaryInverse":"#de000000","colorTextSecondary":"#b3ffffff","colorTextSecondaryInverse":"#8a000000","colorDivider":"#1FFFFFFF","colorForeground":"#FFFFFF","colorForegroundHighlight":"#FFFFFF","colorForegroundHighlightSecondary":"#B3FFFFFF","colorForegroundSecondary":"#B3FFFFFF","colorForegroundAction":"#448AFF","colorForegroundNotice":"#FFD740","colorForegroundError":"#FF5252","colorBackground":"#303030","colorBackgroundHighlight":"#40ffaf3b","colorBackgroundSecondary":"#424242","colorBackgroundCard":"#424242","colorBackgroundDialog":"#303030","colorMarkerLine":"#B71C1C","colorTintActivity":"#AFB42B","colorTintMessage":"#42A5F5","colorTintHighlight":"#FFAB00","colorTintNotification":"#D32F2F","colorTintSecure":"#4CAF50","colorTintPartiallySecure":"#FFC107","colorTintInsecure":"#D32F2F"},
  "QuasselLight": {"colorPrimary":"#0a70c0","colorPrimaryDark":"#105a94","colorAccent":"#ffaf3b","senderColor0":"#cc0000","senderColor1":"#006cad","senderColor2":"#4d9900","senderColor3":"#6600cc","senderColor4":"#a67d00","senderColor5":"#009927","senderColor6":"#0030c0","senderColor7":"#cc009a","senderColor8":"#b94600","senderColor9":"#869900","senderColorA":"#149900","senderColorB":"#009960","senderColorC":"#006cad","senderColorD":"#0099cc","senderColorE":"#b300cc","senderColorF":"#cc004d","colorTextToolbarPrimary":"#ffffff","colorTextToolbarSecondary":"#b2ffffff","colorTextPrimary":"#de000000","colorTextPrimaryInverse":"#ffffffff","colorTextSecondary":"#8a000000","colorTextSecondaryInverse":"#b3ffffff","colorDivider":"#1F000000","colorForeground":"#DE000000","colorForegroundHighlight":"#DE000000","colorForegroundHighlightSecondary":"#8A000000","colorForegroundSecondary":"#8A000000","colorForegroundAction":"#1a237e","colorForegroundNotice":"#916409","colorForegroundError":"#800000","colorBackground":"#FAFAFA","colorBackgroundHighlight":"#ffaf3b","colorBackgroundSecondary":"#F0F0F0","colorBackgroundCard":"#FFFFFF","colorBackgroundDialog":"#FAFAFA","colorMarkerLine":"#800000","colorTintActivity":"#88cc33","colorTintMessage":"#2277dd","colorTintHighlight":"#ff8811","colorTintNotification":"#BB2222","colorTintSecure":"#88cc33","colorTintPartiallySecure":"#ffaf3b","colorTintInsecure":"#BB2222",},
  "QuasselDark": {"colorPrimary":"#0a70c0","colorPrimaryDark":"#105a94","colorAccent":"#ffaf3b","senderColor0":"#cc0000","senderColor1":"#006cad","senderColor2":"#4d9900","senderColor3":"#6600cc","senderColor4":"#a67d00","senderColor5":"#009927","senderColor6":"#0030c0","senderColor7":"#cc009a","senderColor8":"#b94600","senderColor9":"#869900","senderColorA":"#149900","senderColorB":"#009960","senderColorC":"#006cad","senderColorD":"#0099cc","senderColorE":"#b300cc","senderColorF":"#cc004d","colorTextToolbarPrimary":"#ffffff","colorTextToolbarSecondary":"#b2ffffff","colorTextPrimary":"#ffffffff","colorTextPrimaryInverse":"#de000000","colorTextSecondary":"#b3ffffff","colorTextSecondaryInverse":"#8a000000","colorDivider":"#1FFFFFFF","colorForeground":"#FFFFFF","colorForegroundHighlight":"#DE000000","colorForegroundHighlightSecondary":"#8A000000","colorForegroundSecondary":"#B3FFFFFF","colorForegroundAction":"#7986cb","colorForegroundNotice":"#916409","colorForegroundError":"#800000","colorBackground":"#303030","colorBackgroundHighlight":"#ffaf3b","colorBackgroundSecondary":"#424242","colorBackgroundCard":"#424242","colorBackgroundDialog":"#303030","colorMarkerLine":"#800000","colorTintActivity":"#88cc33","colorTintMessage":"#2277dd","colorTintHighlight":"#ff8811","colorTintNotification":"#BB2222","colorTintSecure":"#88cc33","colorTintPartiallySecure":"#ffaf3b","colorTintInsecure":"#BB2222"},
  "Amoled": {"colorPrimary":"#000","colorPrimaryDark":"#000","colorAccent":"#ffaf3b","senderColor0":"#e90d7f","senderColor1":"#8e55e9","senderColor2":"#b30e0e","senderColor3":"#17b339","senderColor4":"#58afb3","senderColor5":"#9d54b3","senderColor6":"#b39775","senderColor7":"#3176b3","senderColor8":"#e90d7f","senderColor9":"#8e55e9","senderColorA":"#b30e0e","senderColorB":"#17b339","senderColorC":"#58afb3","senderColorD":"#9d54b3","senderColorE":"#b39775","senderColorF":"#3176b3","colorTextToolbarPrimary":"#ffffff","colorTextToolbarSecondary":"#b2ffffff","colorTextPrimary":"#ffffffff","colorTextPrimaryInverse":"#de000000","colorTextSecondary":"#b3ffffff","colorTextSecondaryInverse":"#8a000000","colorDivider":"#1FFFFFFF","colorForeground":"#FFFFFF","colorForegroundHighlight":"#FFFFFF","colorForegroundHighlightSecondary":"#B3FFFFFF","colorForegroundSecondary":"#B3FFFFFF","colorForegroundAction":"#7986cb","colorForegroundNotice":"#916409","colorForegroundError":"#800000","colorBackground":"#000000","colorBackgroundHighlight":"#40ffaf3b","colorBackgroundSecondary":"#10ffaf3b","colorBackgroundCard":"#000000","colorBackgroundDialog":"#000000","colorMarkerLine":"#800000","colorTintActivity":"#88cc33","colorTintMessage":"#2277dd","colorTintHighlight":"#ff8811","colorTintNotification":"#D32F2F","colorTintSecure":"#88cc33","colorTintPartiallySecure":"#ffaf3b","colorTintInsecure":"#BB2222"},
  "SolarizedLight": {"colorPrimary":"#EEE8D5","colorPrimaryDark":"#b0ac9e","colorAccent":"#B58900","senderColor0":"#B58900","senderColor1":"#CB4B16","senderColor2":"#DC322f","senderColor3":"#D33682","senderColor4":"#6C71C4","senderColor5":"#268BD2","senderColor6":"#2AA198","senderColor7":"#859900","senderColor8":"#D5A920","senderColor9":"#EB6B36","senderColorA":"#FC524f","senderColorB":"#F356A2","senderColorC":"#8C91E4","senderColorD":"#46ABF2","senderColorE":"#4AC1B8","senderColorF":"#657900","colorTextToolbarPrimary":"#586e75","colorTextToolbarSecondary":"#657b83","colorForeground":"#586e75","colorForegroundHighlight":"#586e75","colorForegroundHighlightSecondary":"#657b83","colorForegroundSecondary":"#657b83","colorForegroundAction":"#268BD2","colorForegroundNotice":"#B58900","colorForegroundError":"#B00000","colorTextPrimary":"#586e75","colorTextPrimaryInverse":"#FDF6E3","colorTextSecondary":"#657b83","colorTextSecondaryInverse":"#839496","colorDivider":"#19586e75","colorBackground":"#FDF6E3","colorBackgroundHighlight":"#40B58900","colorBackgroundSecondary":"#EEE8D5","colorBackgroundCard":"#EEE8D5","colorBackgroundDialog":"#FDF6E3","colorOffline":"#657b83","colorAway":"#657b83","colorMarkerLine":"#CB4B16","colorTintActivity":"#859900","colorTintMessage":"#268BD2","colorTintHighlight":"#EB6B36","colorTintNotification":"#DC322F","colorTintSecure":"#859900","colorTintPartiallySecure":"#D5A920","colorTintInsecure":"#CB4B16"},
  "SolarizedDark": {"colorPrimary":"#073642","colorPrimaryDark":"#002B36","colorAccent":"#B58900","senderColor0":"#B58900","senderColor1":"#CB4B16","senderColor2":"#DC322f","senderColor3":"#D33682","senderColor4":"#6C71C4","senderColor5":"#268BD2","senderColor6":"#2AA198","senderColor7":"#859900","senderColor8":"#D5A920","senderColor9":"#EB6B36","senderColorA":"#FC524f","senderColorB":"#F356A2","senderColorC":"#8C91E4","senderColorD":"#46ABF2","senderColorE":"#4AC1B8","senderColorF":"#657900","colorTextToolbarPrimary":"#93a1a1","colorTextToolbarSecondary":"#839496","colorForeground":"#93a1a1","colorForegroundHighlight":"#93a1a1","colorForegroundHighlightSecondary":"#839496","colorForegroundSecondary":"#839496","colorForegroundAction":"#268BD2","colorForegroundNotice":"#B58900","colorForegroundError":"#B00000","colorTextPrimary":"#93a1a1","colorTextPrimaryInverse":"#002B36","colorTextSecondary":"#839496","colorTextSecondaryInverse":"#657b83","colorDivider":"#1993a1a1","colorBackground":"#002B36","colorBackgroundHighlight":"#30268BD2","colorBackgroundSecondary":"#073642","colorBackgroundCard":"#073642","colorBackgroundDialog":"#002B36","colorOffline":"#839496","colorAway":"#839496","colorMarkerLine":"#CB4B16","colorTintActivity":"#859900","colorTintMessage":"#268BD2","colorTintHighlight":"#EB6B36","colorTintNotification":"#DC322F","colorTintSecure":"#859900","colorTintPartiallySecure":"#B58900","colorTintInsecure":"#CB4B16"},
  "GruvboxLight": {"colorPrimary":"#ebdbb2","colorPrimaryDark":"#b6ae91","colorAccent":"#d65d0e","senderColor0":"#d65d0e","senderColor1":"#cc241d","senderColor2":"#98971a","senderColor3":"#d79921","senderColor4":"#458588","senderColor5":"#b16286","senderColor6":"#689d6a","senderColor7":"#7c6f64","senderColor8":"#928374","senderColor9":"#9d0006","senderColorA":"#79740e","senderColorB":"#b57614","senderColorC":"#076678","senderColorD":"#8f3f71","senderColorE":"#427b58","senderColorF":"#af3a03","colorTextToolbarPrimary":"#3c3836","colorTextToolbarSecondary":"#665c54","colorForeground":"#3c3836","colorForegroundHighlight":"#3c3836","colorForegroundHighlightSecondary":"#665c54","colorForegroundSecondary":"#665c54","colorForegroundAction":"#076678","colorForegroundNotice":"#b57614","colorForegroundError":"#9d0006","colorTextPrimary":"#3c3836","colorTextPrimaryInverse":"#fbf1c7","colorTextSecondary":"#665c54","colorTextSecondaryInverse":"#bdae93","colorDivider":"#2d928374","colorBackground":"#fbf1c7","colorBackgroundHighlight":"#40d65d0e","colorBackgroundSecondary":"#ebdbb2","colorBackgroundCard":"#ebdbb2","colorBackgroundDialog":"#f2d5bc","colorOffline":"#665c54","colorAway":"#665c54","colorMarkerLine":"#9d0006","colorTintActivity":"#98971a","colorTintMessage":"#458588","colorTintHighlight":"#d65d0e","colorTintNotification":"#cc241d","colorTintSecure":"#98971a","colorTintPartiallySecure":"#d79921","colorTintInsecure":"#9d0006"},
  "GruvboxDark": {"colorPrimary":"#504945","colorPrimaryDark":"#3c3734","colorAccent":"#d65d0e","senderColor0":"#d65d0e","senderColor1":"#cc241d","senderColor2":"#98971a","senderColor3":"#d79921","senderColor4":"#458588","senderColor5":"#b16286","senderColor6":"#689d6a","senderColor7":"#a89984","senderColor8":"#928374","senderColor9":"#fb4934","senderColorA":"#b8bb26","senderColorB":"#fabd2f","senderColorC":"#83a598","senderColorD":"#d3869b","senderColorE":"#8ec07c","senderColorF":"#fe8019","colorTextToolbarPrimary":"#dbdbb2","colorTextToolbarSecondary":"#bdae93","colorForeground":"#dbdbb2","colorForegroundHighlight":"#dbdbb2","colorForegroundHighlightSecondary":"#bdae93","colorForegroundSecondary":"#bdae93","colorForegroundAction":"#458588","colorForegroundNotice":"#d79921","colorForegroundError":"#cc241d","colorTextPrimary":"#dbdbb2","colorTextPrimaryInverse":"#282828","colorTextSecondary":"#bdae93","colorTextSecondaryInverse":"#665c54","colorDivider":"#33928374","colorBackground":"#282828","colorBackgroundHighlight":"#40d65d0e","colorBackgroundSecondary":"#504945","colorBackgroundCard":"#504945","colorBackgroundDialog":"#3c3836","colorOffline":"#bdae93","colorAway":"#bdae93","colorMarkerLine":"#cc241d","colorTintActivity":"#98971a","colorTintMessage":"#458588","colorTintHighlight":"#d65d0e","colorTintNotification":"#cc241d","colorTintSecure":"#98971a","colorTintPartiallySecure":"#d79921","colorTintInsecure":"#cc241d"},
  "Dracula": {"colorPrimary":"#44475a","colorPrimaryDark":"#282a36","colorAccent":"#ff79c6","senderColor0":"#6272a4","senderColor1":"#8be9fd","senderColor2":"#50fa7b","senderColor3":"#ffb86c","senderColor4":"#ff79c6","senderColor5":"#bd93f9","senderColor6":"#ff5555","senderColor7":"#f1fa8c","senderColor8":"#6272a4","senderColor9":"#8be9fd","senderColorA":"#50fa7b","senderColorB":"#ffb86c","senderColorC":"#ff79c6","senderColorD":"#bd93f9","senderColorE":"#ff5555","senderColorF":"#f1fa8c","colorTextToolbarPrimary":"#f8f8f2","colorTextToolbarSecondary":"#6272a4","colorForeground":"#f8f8f2","colorForegroundHighlight":"#f8f8f2","colorForegroundHighlightSecondary":"#6272a4","colorForegroundSecondary":"#6272a4","colorForegroundAction":"#50fa7b","colorForegroundNotice":"#44475a","colorForegroundError":"#ff5555","colorTextPrimary":"#f8f8f2","colorTextPrimaryInverse":"#282a36","colorTextSecondary":"#6272a4","colorTextSecondaryInverse":"#586e75","colorDivider":"#19f8f8f2","colorBackground":"#282a36","colorBackgroundHighlight":"#20ff79c6","colorBackgroundSecondary":"#44475a","colorBackgroundCard":"#44475a","colorBackgroundDialog":"#282a36","colorOffline":"#586e75","colorAway":"#586e75","colorMarkerLine":"#586e75","colorTintActivity":"#6272a4","colorTintMessage":"#acb2b7","colorTintHighlight":"#f8f8f2","colorTintNotification":"#ff5555","colorTintSecure":"#50fa7b","colorTintPartiallySecure":"#ffb86c","colorTintInsecure":"#ff5555"}
};

const options = ["avatars", "roundavatars", "highlightself"];

const cssRoot = document.querySelector(".phone");

function hexToRgba(hex, opacity) {
  c= '0x'+hex.substr(1);
  return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255, opacity].join(',')+')';
}

colors.forEach((id) => {
  const inputColor = document.getElementById(id+"-color");
  const inputOpacity = document.getElementById(id+"-opacity");
  
  const change = () => {
    updateColor(id);
  };
  inputColor.addEventListener("change", change);
  inputOpacity.addEventListener("change", change);
  inputOpacity.addEventListener("mousemove", change);
  updateColor(id, true);
});

function updateColor(id, skipSave) {
  const inputColor = document.getElementById(id+"-color");
  const inputOpacity = document.getElementById(id+"-opacity");
  
  const color = inputColor.value;
  const opacity = inputOpacity.value / 255.0;
  cssRoot.style.setProperty("--"+id, hexToRgba(color, opacity));
}

function normalizeColor(value) {
  const data = {
    color: "#000000",
    opacity: 255
  };
  
  if (value.startsWith("#")) {
    if (value.length == 4) {
      data.color = "#"+value[1]+value[1]+value[2]+value[2]+value[3]+value[3];
    } else if (value.length == 5) {
      data.color = "#"+value[2]+value[2]+value[3]+value[3]+value[4]+value[4];
      data.opacity = parseInt(value[1]+value[1], 16);
    } else if (value.length == 7) {
      data.color = value;
    } else if (value.length == 9) {
      data.color = "#"+value.substr(3);
      data.opacity = parseInt(value.substr(1,2), 16);
    }
  }
  return data;
}

function load(json) {
  colors.forEach((id) => {
    const inputColor = document.getElementById(id+"-color");
    const inputOpacity = document.getElementById(id+"-opacity");
    
    const data = normalizeColor(json[id] || "");
    
    inputColor.value = data.color;
    inputOpacity.value = data.opacity;
    
    updateColor(id, true);
  });
}

function save() {
  const json = {};
  colors.forEach((id) => {
    const inputColor = document.getElementById(id+"-color");
    const inputOpacity = document.getElementById(id+"-opacity");
    
    const color = inputColor.value;
    const opacity = +inputOpacity.value;
    
    if (opacity == 255) {
      json[id] = color;
    } else {
      opacityString = ("00"+opacity.toString(16)).substr(-2);
      json[id] = "#"+opacityString+color.substr(1);
    }
  });
  return json;
}

function download(filename, content) {
    const element = document.createElement("a");
    const data = new Blob([content], {type : 'application/json'});
    element.setAttribute("href", URL.createObjectURL(data));
    element.setAttribute("download", filename);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

const data = document.getElementById("data");
const buttonSave = document.getElementById("save");
const inputLoad = document.getElementById("load");

buttonSave.addEventListener("click", () => {
  download("quasseldroid-theme.json", JSON.stringify(save()));
});
inputLoad.addEventListener("change", () => {
  if (inputLoad.files) {
    inputLoad.files[0].text().then(raw => load(JSON.parse(raw)));
  }
});

themes.forEach((theme) => {
  const button = document.getElementById("load"+theme);
  button.addEventListener("click", () => {
    load(themeData[theme]);
  });
});

function updateOption(option, skipSave) {
  const input = document.getElementById(option);  
  if (input.checked) {
    cssRoot.dataset[option] = undefined;
  } else {
    delete cssRoot.dataset[option];
  }
}

options.forEach((option) => {
  const input = document.getElementById(option);  
  input.addEventListener("change", () => {
    updateOption(option);
  });
  updateOption(option, true);
});
